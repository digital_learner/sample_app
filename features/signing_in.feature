Feature: Signing in

  Scenario: Unsuccessful signin
    Given a user visits the signin page
    When (s)he submits invalid signin information
    Then (s)he should see an error message

  Scenario: Successful signin
    Given a user visits the signin page
      And the user has an account
      And the user submits valid signin information
    Then (s)he should see their profile page
      And (s)he should see a signout link
